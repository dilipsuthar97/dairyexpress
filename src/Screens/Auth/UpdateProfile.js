// --------------- LIBRARIES ---------------
import React, { useState } from 'react';
import { TextInput, View, Text } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useSelector } from 'react-redux';

// --------------- ASSETS ---------------
import { Colors, Constants, Matrics } from '../../CommonConfig';
import { Button, MyStatusBar, Loader } from '../../Components/Common';
import { Navigation, Popup } from '../../Helpers';

// --------------- FUNCTION DECLARATION ---------------
const Login = () => {
    // --------------- REDUCER STATE ---------------
    const {
        Common: { isConnected },
    } = useSelector((state) => state);

    // --------------- STATE ---------------
    const [phoneNo, setPhoneNo] = useState('');
    // const [email, setEmail] = useState('');
    // const [password, setPassword] = useState('');
    // const [securePassword, setSecurePassword] = useState(true);
    // const [emailError, setEmailError] = useState('');
    // const [passwordError, setPasswordError] = useState('');

    const [isLoadingLogin, setLoginFlag] = useState(false);

    // --------------- LIFECYCLE ---------------
    // --------------- METHODS ---------------
    /**
     * Input methods - start
     */
    // const onTogglePassword = () => setSecurePassword(!securePassword);
    // const onChangeEmail = (text) => setEmail(text);
    // const onChangePassword = (text) => setPassword(text);
    /**
     * Input methods - end
     */

    /**
     * Validate form - start
     */
    // const validateForm = () => {
    //     let validate = true;

    //     if (email === '') {
    //         validate = false;
    //         setEmailError(Constants.EMAIL_ERROR.EMPTY);
    //     } else if (!Validator.validateEmail(email)) {
    //         validate = false;
    //         setEmailError(Constants.EMAIL_ERROR.INVALID);
    //     } else {
    //         setEmailError('');
    //     }

    //     if (password === '') {
    //         validate = false;
    //         setPasswordError(Constants.PASSWORD_ERROR.EMPTY);
    //     } else {
    //         setPasswordError('');
    //     }

    //     return validate;
    // };
    /**
     * Validate form - end
     */

    const onPressContinue = async () => {
        if (phoneNo.length <= 0) {
            Popup.info('Please add phone number.');
            return;
        }

        if (isConnected) {
            Popup.error('Please connect to a valid internet connection.');
            return;
        }

        setLoginFlag(true);
        setTimeout(() => {
            setLoginFlag(false);
            Navigation.navigate('Root');
        }, 1500);
    };

    // --------------- UI METHODS ---------------
    // --------------- RENDER ---------------
    return (
        <View
            style={{
                flex: 1,
                backgroundColor: Colors.WHITE,
            }}>
            <MyStatusBar />
            <View
                style={{
                    height: Matrics.screenHeight * 0.3,
                    backgroundColor: Colors.SURFACE,
                    width: '100%',
                    justifyContent: 'flex-end',
                    paddingHorizontal: Matrics.s(50),
                }}>
                <Text
                    style={{
                        fontSize: Matrics.mvs(34),
                        fontWeight: 'bold',
                    }}>
                    {'Login'}
                </Text>
                <Text
                    style={{
                        fontSize: Matrics.mvs(11),
                        color: Colors.GRAY,
                        paddingVertical: Matrics.vs(12),
                    }}>
                    {'Enter Your Phone Number to Continue'}
                </Text>
            </View>
            <View
                style={{
                    flex: 1,
                    paddingHorizontal: Matrics.s(50),
                    paddingTop: Matrics.vs(16),
                }}>
                <View
                    style={{
                        borderBottomColor: Colors.OVERLAY_DARK_20,
                        borderBottomWidth: 1,
                    }}>
                    <Text
                        style={{
                            color: Colors.MATEBLACK,
                        }}>
                        {'Phone Number'}
                    </Text>
                    <View
                        style={{
                            flexDirection: 'row',
                            paddingBottom: Matrics.vs(8),
                        }}>
                        <Text
                            style={{
                                fontSize: Matrics.mvs(32),
                                color: Colors.MATEBLACK,
                            }}>
                            {'+91 - '}
                        </Text>
                        <TextInput
                            style={{
                                fontSize: Matrics.mvs(32),
                                paddingTop: 0,
                                paddingBottom: 0,
                                color: Colors.MATEBLACK,
                                flex: 1,
                            }}
                            maxLength={10}
                            keyboardType={'number-pad'}
                            onChangeText={(text) => setPhoneNo(text)}
                        />
                    </View>
                </View>
                <Button
                    label={'Continue'}
                    style={{
                        marginTop: Matrics.vs(32),
                        height: Matrics.vs(50),
                    }}
                    textStyle={{
                        fontSize: Matrics.mvs(18),
                    }}
                    onPress={onPressContinue}
                />
            </View>
            {/* <View style={styles.view}>
                <Text style={styles.logoText}>Dairy Express</Text>
            </View>
            <View style={styles.contentWrapper(insets.bottom)}>
                <Input
                    label='Email'
                    placeholder={'Your email address'}
                    errorMsg={emailError}
                    value={email}
                    onChangeText={onChangeEmail}
                    keyboardType={'email-address'}
                />
                <Input
                    label='Password'
                    placeholder={'Your password'}
                    errorMsg={passwordError}
                    value={password}
                    showPasswordToggle={true}
                    secureTextEntry={securePassword}
                    onPasswordToggle={onTogglePassword}
                    onChangeText={onChangePassword}
                    style={styles.passwordInput}
                />
                <Button
                    label='Login'
                    style={styles.button}
                    onPress={onPressLogin}
                />
                <View style={styles.signupWrapper}>
                    <Text style={styles.signupLabel}>
                        {"Don't have an account? "}
                    </Text>
                    <NavLink
                        label='Sign Up'
                        onPress={onPressSignup}
                        fontSize={13}
                        color={Colors.PRIMARY_COLOR}
                        textDecorationColor={Colors.PRIMARY_COLOR}
                    />
                </View>
            </View> */}
            <Loader visible={isLoadingLogin} />
        </View>
    );
};

export default Login;
