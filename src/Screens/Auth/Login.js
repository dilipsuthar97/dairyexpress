// --------------- LIBRARIES ---------------
import React, { useState } from 'react';
import { TextInput, View, Text } from 'react-native';
import { useSelector } from 'react-redux';

// --------------- ASSETS ---------------
import { Colors, Matrics } from '../../CommonConfig';
import { Button, MyStatusBar, Loader } from '../../Components/Common';
import { Navigation, Popup } from '../../Helpers';

// --------------- FUNCTION DECLARATION ---------------
const Login = () => {
    // --------------- REDUCER STATE ---------------
    const {
        Common: { isConnected },
    } = useSelector((state) => state);

    // --------------- STATE ---------------
    const [phoneNo, setPhoneNo] = useState('');

    const [isLoadingLogin, setLoginFlag] = useState(false);

    // --------------- LIFECYCLE ---------------
    // --------------- METHODS ---------------
    const onPressContinue = async () => {
        if (phoneNo.length != 10) {
            Popup.info('Please add phone number.');
            return;
        }

        if (!isConnected) {
            Popup.error('Please connect to a valid internet connection.');
            return;
        }

        setLoginFlag(true);
        setTimeout(() => {
            setLoginFlag(false);
            Navigation.navigate('Verification', { phoneNo });
        }, 1500);
    };

    // --------------- UI METHODS ---------------
    // --------------- RENDER ---------------
    return (
        <View
            style={{
                flex: 1,
                backgroundColor: Colors.WHITE,
            }}>
            <MyStatusBar />
            <View
                style={{
                    height: Matrics.screenHeight * 0.3,
                    backgroundColor: Colors.SURFACE,
                    width: '100%',
                    justifyContent: 'flex-end',
                    paddingHorizontal: Matrics.s(50),
                }}>
                <Text
                    style={{
                        fontSize: Matrics.mvs(34),
                        fontWeight: 'bold',
                    }}>
                    {'Login'}
                </Text>
                <Text
                    style={{
                        fontSize: Matrics.mvs(11),
                        color: Colors.GRAY,
                        paddingVertical: Matrics.vs(12),
                    }}>
                    {'Enter Your Phone Number to Continue'}
                </Text>
            </View>
            <View
                style={{
                    flex: 1,
                    paddingHorizontal: Matrics.s(50),
                    paddingTop: Matrics.vs(16),
                }}>
                <View
                    style={{
                        borderBottomColor: Colors.OVERLAY_DARK_20,
                        borderBottomWidth: 1,
                    }}>
                    <Text
                        style={{
                            color: Colors.MATEBLACK,
                        }}>
                        {'Phone Number'}
                    </Text>
                    <View
                        style={{
                            flexDirection: 'row',
                            paddingBottom: Matrics.vs(8),
                        }}>
                        <Text
                            style={{
                                fontSize: Matrics.mvs(32),
                                color: Colors.MATEBLACK,
                            }}>
                            {'+91 - '}
                        </Text>
                        <TextInput
                            style={{
                                fontSize: Matrics.mvs(32),
                                paddingTop: 0,
                                paddingBottom: 0,
                                color: Colors.MATEBLACK,
                                flex: 1,
                            }}
                            maxLength={10}
                            keyboardType={'number-pad'}
                            value={phoneNo}
                            onChangeText={(text) => setPhoneNo(text)}
                        />
                    </View>
                </View>
                <Button
                    label={'Continue'}
                    style={{
                        marginTop: Matrics.vs(32),
                        height: Matrics.vs(50),
                    }}
                    textStyle={{
                        fontSize: Matrics.mvs(18),
                    }}
                    onPress={onPressContinue}
                />
            </View>
            <Loader visible={isLoadingLogin} />
        </View>
    );
};

export default Login;
