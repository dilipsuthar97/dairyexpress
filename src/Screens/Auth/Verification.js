// --------------- LIBRARIES ---------------
import React, { useState } from 'react';
import { TextInput, View, Text } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

// --------------- ASSETS ---------------
import { Colors, Matrics } from '../../CommonConfig';
import { Button, MyStatusBar, Loader } from '../../Components/Common';
import { Navigation, Popup } from '../../Helpers';
import { login } from '../../Redux/Actions';

// --------------- FUNCTION DECLARATION ---------------
const Verification = ({ route: { params } }) => {
    // --------------- REDUCER STATE ---------------
    const {
        Common: { isConnected },
    } = useSelector((state) => state);
    const dispatch = useDispatch();

    // --------------- STATE ---------------
    const [code, setCode] = useState('');

    const [isLoadingLogin, setLoginFlag] = useState(false);

    // --------------- LIFECYCLE ---------------
    // --------------- METHODS ---------------
    const onPressContinue = async () => {
        if (code.length != 4) {
            Popup.info('Please add verification code.');
            return;
        }

        if (!isConnected) {
            Popup.error('Please connect to a valid internet connection.');
            return;
        }

        setLoginFlag(true);
        setTimeout(() => {
            setLoginFlag(false);
            dispatch(login.Request({ phone_no: `+91 ${params.phoneNo}` }));
            Navigation.reset(1, [{ name: 'Root' }]);
        }, 1500);
    };

    // --------------- UI METHODS ---------------
    // --------------- RENDER ---------------
    return (
        <View
            style={{
                flex: 1,
                backgroundColor: Colors.WHITE,
            }}>
            <MyStatusBar />
            <View
                style={{
                    height: Matrics.screenHeight * 0.3,
                    backgroundColor: Colors.SURFACE,
                    width: '100%',
                    justifyContent: 'flex-end',
                    paddingHorizontal: Matrics.s(50),
                }}>
                <Text
                    style={{
                        fontSize: Matrics.mvs(34),
                        fontWeight: 'bold',
                    }}>
                    {'Login'}
                </Text>
                <Text
                    style={{
                        fontSize: Matrics.mvs(11),
                        color: Colors.GRAY,
                        paddingVertical: Matrics.vs(12),
                    }}>
                    {'Enter Verification Code'}
                </Text>
            </View>
            <View
                style={{
                    flex: 1,
                    paddingHorizontal: Matrics.s(50),
                    paddingTop: Matrics.vs(16),
                }}>
                <View
                    style={{
                        borderBottomColor: Colors.OVERLAY_DARK_20,
                        borderBottomWidth: 1,
                    }}>
                    <Text
                        style={{
                            color: Colors.MATEBLACK,
                        }}>
                        {'Verification'}
                    </Text>
                    <TextInput
                        style={{
                            fontSize: Matrics.mvs(22),
                            paddingTop: Matrics.vs(16),
                            paddingBottom: Matrics.vs(8),
                            color: Colors.MATEBLACK,
                            textAlign: 'center',
                        }}
                        maxLength={4}
                        keyboardType={'number-pad'}
                        value={code}
                        onChangeText={(text) => setCode(text)}
                    />
                </View>
                <Button
                    label={'Continue'}
                    style={{
                        marginTop: Matrics.vs(32),
                        height: Matrics.vs(50),
                    }}
                    textStyle={{
                        fontSize: Matrics.mvs(18),
                    }}
                    onPress={onPressContinue}
                />
            </View>
            <Loader visible={isLoadingLogin} />
        </View>
    );
};

export default Verification;
