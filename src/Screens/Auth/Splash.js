// --------------- LIBRARIES ---------------
import React, { useEffect } from 'react';
import { View, Text, Image } from 'react-native';
import { CommonActions } from '@react-navigation/native';
import { useSelector } from 'react-redux';

// --------------- ASSETS ---------------
import { Colors, Matrics, Images } from '../../CommonConfig';
import { MyStatusBar } from '../../Components/Common';

// --------------- FUNCTION DECLARATION ---------------
const Splash = ({ navigation }) => {
    // --------------- REDUCER STATE ---------------
    const { Auth } = useSelector((state) => state);

    // --------------- STATE ---------------
    // --------------- LIFECYCLE ---------------
    useEffect(() => {
        setTimeout(() => {
            diveIntoApp();
        }, 1000);
    }, []);

    const diveIntoApp = async () => {
        // Check if user already logged-in
        if (Auth?.data?.isAuthenticated) {
            navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [{ name: 'Root' }],
                }),
            );
        } else {
            navigation.dispatch(
                CommonActions.reset({
                    index: 0,
                    routes: [{ name: 'Onboarding' }],
                }),
            );
        }
    };

    // --------------- METHODS ---------------
    // --------------- UI METHODS ---------------
    // --------------- RENDER ---------------
    return (
        <View
            style={{
                flex: 1,
                backgroundColor: Colors.LOGO_BG,
                justifyContent: 'center',
                alignItems: 'center',
            }}>
            <MyStatusBar barStyle={'light-content'} />
            <Image
                source={Images.LOGO}
                style={{
                    width: Matrics.screenWidth,
                    height: Matrics.screenWidth * 0.7,
                    resizeMode: 'contain',
                }}
            />
        </View>
    );
};

export default Splash;
