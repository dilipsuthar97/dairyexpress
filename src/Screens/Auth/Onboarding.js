// --------------- LIBRARIES ---------------
import * as React from 'react';
import { View, Animated, Text, Image, TouchableOpacity } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';

// --------------- ASSETS ---------------
import { Images, Matrics, Colors } from '../../CommonConfig';
import { MyStatusBar, NavLink } from '../../Components/Common';
import { Navigation } from '../../Helpers';

// --------------- FUNCTION DECLARATION ---------------
const Onboarding = () => {
    const data = [
        {
            image: Images.INTRO_1,
            title: 'Order at 11PM & Get it by 7AM',
            desc:
                'Order at 11 pm, and we will deliver it by 7 am the next day.',
        },
        {
            image: Images.INTRO_2,
            title: 'Delivered quickly to your doorstep',
            desc:
                'Get one time quick delivery!\nSubscribe our remium package and get offers and much more.',
        },
        {
            image: Images.INTRO_3,
            title: 'Your safety is our priority',
            desc:
                'Because of the pandemic Covid-19, we take every precaution before delivering anything.',
        },
    ];
    const insets = useSafeAreaInsets();
    const scrollX = React.useRef(new Animated.Value(0)).current;
    const color = scrollX.interpolate({
        inputRange: [0, Matrics.screenWidth, 2 * Matrics.screenWidth],
        outputRange: [Colors.COLOR_1, Colors.COLOR_2, Colors.COLOR_3],
    });

    // --------------- STATE ---------------
    // --------------- LIFECYCLE ---------------
    // --------------- METHODS ---------------
    // --------------- UI METHODS ---------------
    const _renderItem = ({ item, index }) => {
        return (
            <View
                style={{
                    width: Matrics.screenWidth,
                    flex: 1,
                    alignItems: 'center',
                    paddingTop: insets.top + Matrics.vs(8),
                }}>
                <Image
                    style={{
                        width: Matrics.screenWidth * 0.85,
                        height: '60%',
                        resizeMode: 'contain',
                        flex: 1,
                    }}
                    source={item.image}
                />
                <Text
                    style={{
                        fontSize: Matrics.mvs(24),
                        fontWeight: 'bold',
                        paddingHorizontal: Matrics.s(60),
                        textAlign: 'center',
                        marginBottom: Matrics.vs(8),
                    }}>
                    {item.title}
                </Text>
                <Text
                    style={{
                        fontSize: Matrics.mvs(14),
                        paddingHorizontal: Matrics.s(60),
                        textAlign: 'center',
                        lineHeight: Matrics.mvs(22),
                    }}>
                    {item.desc}
                </Text>
            </View>
        );
    };

    // --------------- RENDER ---------------
    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            <MyStatusBar />
            <Animated.FlatList
                horizontal
                pagingEnabled
                showsHorizontalScrollIndicator={false}
                onScroll={Animated.event(
                    [
                        {
                            nativeEvent: { contentOffset: { x: scrollX } },
                        },
                    ],
                    { useNativeDriver: false },
                )}
                data={data}
                keyExtractor={(_, index) => index.toString()}
                renderItem={_renderItem}
            />
            <View
                style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    paddingBottom: Matrics.vs(16),
                    paddingTop: Matrics.vs(22),
                }}>
                <TouchableOpacity
                    activeOpacity={0.5}
                    delayPressIn={0}
                    onPress={() => Navigation.navigate('SelectLocation')}>
                    <Animated.View
                        style={{
                            backgroundColor: color,
                            paddingHorizontal: Matrics.s(18),
                            paddingVertical: Matrics.vs(12),
                            elevation: 2,
                            borderRadius: Matrics.mvs(4),
                            marginBottom: Matrics.vs(16),
                        }}>
                        <Text
                            style={{
                                fontSize: Matrics.mvs(18),
                                fontWeight: 'bold',
                                color: Colors.WHITE,
                            }}>
                            {'Setup Location'}
                        </Text>
                    </Animated.View>
                </TouchableOpacity>
                <View
                    style={{
                        flexDirection: 'row',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}>
                    <Text
                        style={{
                            fontSize: Matrics.mvs(13),
                            color: Colors.BLACK,
                        }}>
                        {'Already have an account? '}
                    </Text>
                    <NavLink
                        label='Log in!'
                        onPress={() => Navigation.navigate('Login')}
                        fontSize={13}
                        color={color}
                        textDecorationColor={color}
                    />
                </View>
            </View>
        </View>
    );
};

export default Onboarding;
