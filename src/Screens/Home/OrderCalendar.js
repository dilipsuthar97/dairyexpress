// --------------- LIBRARIES ---------------
import * as React from 'react';
import { View, TouchableOpacity, TextInput, Text } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import ICFeather from 'react-native-vector-icons/Feather';
import CalendarStrip from 'react-native-calendar-strip';

// --------------- ASSETS ---------------
import { Matrics, Colors } from '../../CommonConfig';
import { MyStatusBar, EmptyUI } from '../../Components/Common';
import { Navigation } from '../../Helpers';

// --------------- FUNCTION DECLARATION ---------------
const OrderCalendar = () => {
    // --------------- STATE ---------------
    // --------------- LIFECYCLE ---------------
    // --------------- METHODS ---------------
    // --------------- UI METHODS ---------------
    // --------------- RENDER ---------------
    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            <MyStatusBar />
            <SafeAreaView
                style={{
                    backgroundColor: Colors.SURFACE,
                }}>
                <View
                    style={{
                        height: Matrics.vs(55),
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: Matrics.s(16),
                    }}>
                    <TouchableOpacity onPress={() => Navigation.goBack()}>
                        <ICFeather
                            name={'arrow-left'}
                            size={24}
                            color={Colors.BLACK}
                        />
                    </TouchableOpacity>
                    <Text
                        style={{
                            color: Colors.BLACK,
                            fontSize: Matrics.mvs(18),
                            paddingLeft: Matrics.s(8),
                            paddingRight: Matrics.s(8),
                            flex: 1,
                        }}>
                        {'Order Calendar'}
                    </Text>
                </View>
            </SafeAreaView>
            <CalendarStrip
                scrollable
                style={{ height: Matrics.vs(80) }}
                calendarColor={Colors.SURFACE}
                calendarHeaderStyle={{ color: Colors.GRAY }}
                dateNumberStyle={{ color: Colors.GRAY }}
                dateNameStyle={{ color: Colors.GRAY }}
                selectedDate={new Date()}
                iconContainer={{ flex: 0.1 }}
                highlightDateNumberStyle={{ color: Colors.PRIMARY_COLOR }}
                highlightDateNameStyle={{ color: Colors.PRIMARY_COLOR }}
            />
            <EmptyUI title={'No orders'} />
        </View>
    );
};

export default OrderCalendar;
