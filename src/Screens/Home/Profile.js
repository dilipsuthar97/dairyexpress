// --------------- LIBRARIES ---------------
import * as React from 'react';
import {
    Text,
    View,
    ScrollView,
    TextInput,
    TouchableOpacity,
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import ICFeather from 'react-native-vector-icons/Feather';
import { useSelector } from 'react-redux';

// --------------- ASSETS ---------------
import { Colors, Constants, Matrics } from '../../CommonConfig';
import { Button, MyStatusBar } from '../../Components/Common';
import { Navigation, Popup, Validator } from '../../Helpers';

// --------------- FUNCTION DECLARATION ---------------
const ScreenName = () => {
    // --------------- REDUCER STATE ---------------
    const {
        Common: { isConnected },
    } = useSelector((state) => state);

    // --------------- STATE ---------------
    const [fullname, setFullname] = React.useState('');
    const [email, setEmail] = React.useState('');

    // --------------- LIFECYCLE ---------------
    // --------------- METHODS ---------------
    const validate = () => {
        let validate = true;

        if (fullname.length <= 0) {
            validate = false;
            Popup.info(Constants.FULLNAME_ERROR.EMPTY);
        } else if (email.length <= 0) {
            validate = false;
            Popup.info(Constants.EMAIL_ERROR.EMPTY);
        } else if (!Validator.validateEmail(email)) {
            validate = false;
            Popup.info(Constants.EMAIL_ERROR.INVALID);
        }

        return validate;
    };

    const onPressContinue = () => {
        if (!validate()) {
            return;
        }

        if (!isConnected) {
            Popup.error('Please connect to a valid internet connection.');
            return;
        }

        Navigation.goBack();
    };

    // --------------- UI METHODS ---------------
    // --------------- RENDER ---------------
    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            <MyStatusBar />
            <SafeAreaView style={{ backgroundColor: Colors.SURFACE }}>
                <View
                    style={{
                        height: Matrics.vs(100),
                        justifyContent: 'space-evenly',
                        paddingHorizontal: Matrics.s(16),
                    }}>
                    <View
                        style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <TouchableOpacity onPress={() => Navigation.goBack()}>
                            <ICFeather
                                name={'arrow-left'}
                                size={32}
                                color={Colors.BLACK}
                            />
                        </TouchableOpacity>
                        <Text
                            style={{
                                fontSize: Matrics.mvs(32),
                                fontWeight: 'bold',
                                marginLeft: Matrics.s(8),
                            }}>
                            {'Update Your Profile'}
                        </Text>
                    </View>
                    <Text
                        style={{
                            fontSize: Matrics.mvs(11),
                            color: Colors.GRAY,
                            marginLeft: Matrics.s(40),
                        }}>
                        {'Enter your details'}
                    </Text>
                </View>
            </SafeAreaView>
            <ScrollView style={{ paddingHorizontal: Matrics.s(16) }}>
                <View style={{ marginVertical: Matrics.vs(16) }}>
                    <Text
                        style={{
                            fontSize: Matrics.mvs(22),
                            fontWeight: '600',
                        }}>
                        {'Full Name'}
                    </Text>
                    <TextInput
                        style={{
                            fontSize: Matrics.mvs(18),
                            borderBottomColor: Colors.OVERLAY_DARK_20,
                            borderBottomWidth: 1,
                            paddingTop: Matrics.vs(4),
                            paddingBottom: 0,
                            paddingLeft: 0,
                            paddingRight: 0,
                            color: Colors.GRAY,
                        }}
                        placeholderTextColor={Colors.LIGHT_GRAY}
                        placeholder={'Enter your full name'}
                        value={fullname}
                        onChangeText={(text) => setFullname(text)}
                    />
                </View>
                <View style={{ marginVertical: Matrics.vs(16) }}>
                    <Text
                        style={{
                            fontSize: Matrics.mvs(22),
                            fontWeight: '600',
                        }}>
                        {'Email'}
                    </Text>
                    <TextInput
                        style={{
                            fontSize: Matrics.mvs(18),
                            borderBottomColor: Colors.OVERLAY_DARK_20,
                            borderBottomWidth: 1,
                            paddingTop: Matrics.vs(4),
                            paddingBottom: 0,
                            paddingLeft: 0,
                            paddingRight: 0,
                            color: Colors.GRAY,
                        }}
                        placeholderTextColor={Colors.LIGHT_GRAY}
                        placeholder={'Enter your email'}
                        value={email}
                        onChangeText={(text) => setEmail(text)}
                    />
                </View>
                <Button
                    label={'Continue'}
                    style={{
                        marginTop: Matrics.vs(32),
                        height: Matrics.vs(45),
                        marginHorizontal: Matrics.s(22),
                    }}
                    textStyle={{
                        fontSize: Matrics.mvs(18),
                    }}
                    onPress={onPressContinue}
                />
            </ScrollView>
        </View>
    );
};

export default ScreenName;
