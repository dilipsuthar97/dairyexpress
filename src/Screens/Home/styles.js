import { StyleSheet } from 'react-native';
import { Colors, Matrics, MainStyles } from '../../CommonConfig';

export const homeStyle = StyleSheet.create({
    itemWrpper: {
        width: Matrics.screenWidth - Matrics.mvs(16 * 2),
        flexDirection: 'row',
        backgroundColor: Colors.WHITE,
        marginLeft: Matrics.mvs(16),
        borderRadius: Matrics.mvs(8),
        marginBottom: Matrics.vs(16),
    },
    itemContentWrapper: {
        marginHorizontal: Matrics.s(16),
        flex: 1,
    },
    itemImageWrapper: {
        width: Matrics.vs(85),
        height: Matrics.vs(85),
        borderColor: Colors.OVERLAY_DARK_10,
        borderWidth: 2,
        borderRadius: Matrics.mvs(12),
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemImage: {
        width: Matrics.vs(80),
        height: Matrics.vs(80),
        // resizeMode: 'contain',
        borderRadius: Matrics.mvs(12),
        // backgroundColor: 'red',
    },
    itemName: {
        fontSize: Matrics.mvs(13),
        color: Colors.BLACK,
        marginBottom: Matrics.vs(6),
        fontWeight: 'bold',
    },
    itemDesc: {
        fontSize: Matrics.mvs(12),
        color: Colors.BLACK,
        marginBottom: Matrics.vs(8),
    },
    itemPrice: {
        fontSize: Matrics.mvs(15),
        color: Colors.BLACK,
        marginBottom: Matrics.vs(8),
        fontWeight: 'bold',
        paddingLeft: Matrics.s(16),
    },
    buttonsWrapper: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: Matrics.s(8),
    },
    itemButton: {
        borderColor: Colors.PRIMARY_COLOR,
        borderWidth: 1,
        borderRadius: Matrics.mvs(4),
        paddingVertical: Matrics.vs(2),
        width: Matrics.s(80),
    },
    itemButtonLabel: {
        fontSize: Matrics.mvs(13),
        color: Colors.PRIMARY_COLOR,
        textAlign: 'center',
        fontWeight: 'bold',
    },
});
