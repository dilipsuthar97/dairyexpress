// --------------- LIBRARIES ---------------
import * as React from 'react';
import { View, TouchableOpacity, Image, Text, ScrollView } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import ICFeather from 'react-native-vector-icons/Feather';
import { useSelector, useDispatch } from 'react-redux';

// --------------- ASSETS ---------------
import { homeStyle as styles } from './styles';
import { Matrics, Colors } from '../../CommonConfig';
import { MyStatusBar, EmptyUI, Button, Loader } from '../../Components/Common';
import { Navigation, Popup } from '../../Helpers';
import { removeFromCart, clearCart } from '../../Redux/Actions';

// --------------- FUNCTION DECLARATION ---------------
const Checkout = () => {
    // --------------- REDUCER STATE ---------------
    const {
        Common: { cart },
    } = useSelector((state) => state);
    const dispatch = useDispatch();

    // --------------- STATE ---------------
    const [isLoading, setLoadingFlag] = React.useState(false);

    // --------------- LIFECYCLE ---------------
    // --------------- METHODS ---------------
    const onPressRemove = (item) => {
        dispatch(removeFromCart.Request(item));
    };

    const onPressCheckout = () => {
        setLoadingFlag(true);
        setTimeout(() => {
            Popup.success('Order placed successfully.');
            Navigation.goBack();
            dispatch(clearCart.Request());
        }, 1500);
    };

    // --------------- UI METHODS ---------------
    // --------------- RENDER ---------------
    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            <MyStatusBar />
            <SafeAreaView
                style={{
                    backgroundColor: Colors.SURFACE,
                }}>
                <View
                    style={{
                        height: Matrics.vs(55),
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: Matrics.s(16),
                    }}>
                    <TouchableOpacity onPress={() => Navigation.goBack()}>
                        <ICFeather
                            name={'arrow-left'}
                            size={24}
                            color={Colors.BLACK}
                        />
                    </TouchableOpacity>
                    <Text
                        style={{
                            color: Colors.BLACK,
                            fontSize: Matrics.mvs(18),
                            paddingLeft: Matrics.s(8),
                            paddingRight: Matrics.s(8),
                            flex: 1,
                        }}>
                        {'Checkout'}
                    </Text>
                </View>
            </SafeAreaView>
            {cart?.length <= 0 ? (
                <EmptyUI title={'No items'} />
            ) : (
                <View style={{ flex: 1 }}>
                    <ScrollView style={{ paddingVertical: Matrics.vs(16) }}>
                        {cart.map((product, index) => {
                            return (
                                <View style={styles.itemWrpper} key={index}>
                                    <View style={styles.itemImageWrapper}>
                                        <Image
                                            source={product.image}
                                            style={styles.itemImage}
                                        />
                                    </View>
                                    <View style={styles.itemContentWrapper}>
                                        <Text
                                            numberOfLines={1}
                                            style={styles.itemName}>
                                            {product.name}
                                        </Text>
                                        <Text style={styles.itemDesc}>
                                            {product.desc}
                                        </Text>
                                        <Text style={styles.itemPrice}>
                                            {Number(product.price).toFixed(2)}
                                        </Text>
                                        <View style={styles.buttonsWrapper}>
                                            <TouchableOpacity
                                                onPress={() =>
                                                    onPressRemove(product)
                                                }
                                                style={styles.itemButton}>
                                                <Text
                                                    style={
                                                        styles.itemButtonLabel
                                                    }>
                                                    {'Remove'}
                                                </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            );
                        })}
                    </ScrollView>
                    <Button
                        onPress={onPressCheckout}
                        label={`Checkout for ${cart
                            .map((c) => c.price)
                            .reduce((a, b) => a + b, 0)
                            .toFixed(2)} ₹`}
                        style={{
                            margin: Matrics.mvs(16),
                        }}
                    />
                    <Loader visible={isLoading} />
                </View>
            )}
        </View>
    );
};

export default Checkout;
