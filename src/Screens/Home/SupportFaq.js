// --------------- LIBRARIES ---------------
import * as React from 'react';
import { View, TouchableOpacity, Text, Linking } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import ICFeather from 'react-native-vector-icons/Feather';
import { Toast } from 'react-native-toast-snackbar';

// --------------- ASSETS ---------------
import { Matrics, Colors } from '../../CommonConfig';
import { MyStatusBar } from '../../Components/Common';
import { Navigation } from '../../Helpers';

// --------------- FUNCTION DECLARATION ---------------
const SupportFaq = () => {
    // --------------- REDUCER STATE ---------------
    // --------------- STATE ---------------
    // --------------- LIFECYCLE ---------------
    // --------------- METHODS ---------------
    const onPressCall = async () => {
        const url = `tel:+918888222244`;
        Linking.canOpenURL(url).then(async (isSupport) => {
            if (isSupport) {
                await Linking.openURL(url);
                return;
            }
            Toast.show({
                message: 'Upnable to open phone app',
                duration: Toast.LENGTH_SHORT,
            });
        });
    };

    // --------------- UI METHODS ---------------
    // --------------- RENDER ---------------
    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            <MyStatusBar />
            <SafeAreaView
                style={{
                    backgroundColor: Colors.SURFACE,
                }}>
                <View
                    style={{
                        height: Matrics.vs(55),
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: Matrics.s(16),
                    }}>
                    <TouchableOpacity onPress={() => Navigation.goBack()}>
                        <ICFeather
                            name={'arrow-left'}
                            size={24}
                            color={Colors.BLACK}
                        />
                    </TouchableOpacity>
                    <Text
                        style={{
                            color: Colors.BLACK,
                            fontSize: Matrics.mvs(18),
                            paddingLeft: Matrics.s(8),
                            paddingRight: Matrics.s(8),
                            flex: 1,
                        }}>
                        {'Support & FAQs'}
                    </Text>
                </View>
            </SafeAreaView>
            <View
                style={{
                    padding: Matrics.mvs(16),
                }}>
                <TouchableOpacity
                    onPress={onPressCall}
                    activeOpacity={0.5}
                    delayPressIn={0}>
                    <Text
                        style={{
                            color: Colors.BLACK,
                            fontSize: Matrics.mvs(14),
                        }}>
                        {'Please contact our customer care helpdesk!'}
                    </Text>
                    <Text
                        style={{
                            color: Colors.PRIMARY_COLOR,
                            fontSize: Matrics.mvs(16),
                            fontWeight: 'bold',
                        }}>
                        {'+91 8888222244'}
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
};

export default SupportFaq;
