// --------------- LIBRARIES ---------------
import * as React from 'react';
import {
    View,
    TextInput,
    ScrollView,
    TouchableOpacity,
    Text,
    Image,
} from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import ICFeather from 'react-native-vector-icons/Feather';
import { Snackbar, Toast } from 'react-native-toast-snackbar';

// --------------- ASSETS ---------------
import { Colors, Matrics, Icons } from '../../CommonConfig';
import { MyStatusBar } from '../../Components/Common';
import { Navigation, Popup } from '../../Helpers';

// --------------- FUNCTION DECLARATION ---------------
const SelectLocation = () => {
    // --------------- STATE ---------------
    // --------------- LIFECYCLE ---------------
    // --------------- METHODS ---------------
    // --------------- UI METHODS ---------------
    // --------------- RENDER ---------------
    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            <MyStatusBar />
            <SafeAreaView
                style={{
                    backgroundColor: Colors.SURFACE,
                }}>
                <View
                    style={{
                        height: Matrics.vs(55),
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: Matrics.s(16),
                    }}>
                    <TouchableOpacity onPress={() => Navigation.goBack()}>
                        <ICFeather
                            name={'arrow-left'}
                            size={24}
                            color={Colors.BLACK}
                        />
                    </TouchableOpacity>
                    <TextInput
                        style={{
                            color: Colors.BLACK,
                            fontSize: Matrics.mvs(18),
                            paddingTop: 0,
                            paddingBottom: 0,
                            paddingLeft: Matrics.s(8),
                            paddingRight: Matrics.s(8),
                            flex: 1,
                        }}
                        placeholder={'Enter Your Area, Street Name'}
                        placeholderTextColor={Colors.OVERLAY_DARK_40}
                    />
                    <TouchableOpacity>
                        <ICFeather
                            name={'search'}
                            size={24}
                            color={Colors.BLACK}
                        />
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
            <ScrollView>
                <TouchableOpacity
                    onPress={() => {
                        Snackbar.show({
                            message: 'Location changed.',
                            duration: Snackbar.LENGTH_SHORT,
                            action: {
                                text: 'OK',
                            },
                        });
                        Navigation.goBack();
                    }}
                    delayPressIn={0}
                    activeOpacity={0.5}
                    style={{
                        flexDirection: 'row',
                        paddingLeft: Matrics.s(16),
                    }}>
                    <Image
                        style={{
                            height: Matrics.mvs(50),
                            width: Matrics.mvs(50),
                            resizeMode: 'cover',
                            alignSelf: 'center',
                        }}
                        source={Icons.IC_LOCATION_COVER}
                    />
                    <View
                        style={{
                            borderBottomWidth: 1,
                            borderBottomColor: Colors.OVERLAY_DARK_20,
                            flex: 1,
                            paddingVertical: Matrics.vs(16),
                            paddingRight: Matrics.s(16),
                            marginLeft: Matrics.s(12),
                        }}>
                        <Text
                            style={{
                                fontSize: Matrics.mvs(16),
                                color: Colors.BLACK,
                                fontWeight: 'bold',
                            }}>
                            {'Use Current Location'}
                        </Text>
                        <Text
                            style={{
                                fontSize: Matrics.mvs(13),
                                color: Colors.BLACK,
                            }}>
                            {'Using GPS'}
                        </Text>
                    </View>
                </TouchableOpacity>
            </ScrollView>
        </View>
    );
};

export default SelectLocation;
