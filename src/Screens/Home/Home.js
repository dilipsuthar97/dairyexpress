// --------------- LIBRARIES ---------------
import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    FlatList,
    Image,
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import ICFeather from 'react-native-vector-icons/Feather';

// --------------- ASSETS ---------------
import { homeStyle as styles } from './styles';
import { Icons, Matrics, Colors, Images } from '../../CommonConfig';
import { MyStatusBar, LoaderUI, Loader } from '../../Components/Common';
import { useFilteredData } from '../../Hooks';
import { Navigation } from '../../Helpers';
import Dummy from '../../Utils/Dummy';
import { addToCart, removeFromCart } from '../../Redux/Actions';

// --------------- FUNCTION DECLARATION ---------------
const Home = () => {
    const offers = [
        {
            image: Images.OFFER_1,
        },
        {
            image: Images.OFFER_2,
        },
    ];
    const milestones = [
        {
            icon: Icons.IC_SECURITY,
            text: 'Safety\nMeasures',
        },
        {
            icon: Icons.IC_CALENDAR,
            text: 'Schedule\nYour Delivery',
        },
        {
            icon: Icons.IC_EGGS,
            text: 'Eggs',
        },
        {
            icon: Icons.IC_OFFERS_COVER,
            text: 'Best\nOffers',
        },
    ];

    // --------------- REDUCER STATE ---------------
    const {
        Common: { cart },
    } = useSelector((state) => state);
    const dispatch = useDispatch();

    // --------------- STATE ---------------
    const [search, setSearch] = useState('');
    const [data, setData] = useFilteredData(search, ['name']);
    const [addedItems, setAddedItems] = useState(cart.map((i) => i.id));
    const [isLoading, setLoadingFlag] = useState(false);
    console.log(cart, addedItems);

    // --------------- LIFECYCLE ---------------
    useEffect(() => {
        fetchData();
    }, []);

    useEffect(() => {
        if (cart?.length != addedItems?.length) {
            let newItems = cart.map((i) => i.id);
            setAddedItems(newItems);
        }
    }, [cart]);

    // --------------- METHODS ---------------
    const onPressMenu = () => {
        Navigation.openDrawer();
    };

    const onPressAdd = (item) => {
        dispatch(addToCart.Request(item));
    };

    const onPressRemove = (item) => {
        dispatch(removeFromCart.Request(item));
    };

    const fetchData = () => {
        setLoadingFlag(true);
        setTimeout(() => {
            setData(Dummy.products);
            setLoadingFlag(false);
        }, 2000);
    };

    // --------------- UI METHODS ---------------
    const _renderOffers = ({ item, index }) => {
        return (
            <TouchableOpacity
                activeOpacity={0.5}
                delayPressIn={0}
                style={{
                    width: Matrics.screenWidth * 0.6,
                    height: Matrics.screenWidth * 0.6,
                    backgroundColor: Colors.WHITE,
                    borderRadius: Matrics.mvs(8),
                    marginLeft: Matrics.mvs(16),
                    elevation: 3,
                    overflow: 'hidden',
                }}>
                <Image
                    style={{
                        width: Matrics.screenWidth * 0.6,
                        height: Matrics.screenWidth * 0.6,
                        resizeMode: 'contain',
                    }}
                    source={item.image}
                />
            </TouchableOpacity>
        );
    };

    // --------------- RENDER ---------------
    return (
        <View style={{ flex: 1, backgroundColor: Colors.WHITE }}>
            <MyStatusBar />
            <SafeAreaView
                style={{
                    backgroundColor: Colors.SURFACE,
                    borderBottomColor: Colors.GRAY,
                    borderBottomWidth: 1,
                }}>
                <View
                    style={{
                        flexDirection: 'row',
                        paddingVertical: Matrics.vs(12),
                        paddingHorizontal: Matrics.s(16),
                        alignItems: 'center',
                    }}>
                    <TouchableOpacity
                        delayPressIn={0}
                        activeOpacity={0.5}
                        style={{ marginRight: Matrics.s(16) }}
                        onPress={onPressMenu}>
                        <ICFeather
                            name={'menu'}
                            size={24}
                            color={Colors.PRIMARY_COLOR}
                        />
                    </TouchableOpacity>
                    <TouchableOpacity
                        delayPressIn={0}
                        activeOpacity={0.5}
                        onPress={() => Navigation.navigate('SelectLocation')}>
                        <View
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                            }}>
                            <Text
                                style={{
                                    color: Colors.BLACK,
                                    fontSize: Matrics.mvs(18),
                                    fontWeight: 'bold',
                                }}>
                                {'Other'}
                            </Text>
                            <ICFeather
                                name={'chevron-down'}
                                size={24}
                                color={Colors.PRIMARY_COLOR}
                            />
                        </View>
                        <Text
                            style={{
                                color: Colors.MATEBLACK,
                                fontSize: Matrics.mvs(13),
                            }}>
                            {'Vadodara, Gujarat'}
                        </Text>
                    </TouchableOpacity>
                    <View style={{ flex: 1 }} />
                    <View
                        style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Image
                            style={{
                                width: Matrics.mvs(32),
                                height: Matrics.mvs(32),
                                resizeMode: 'contain',
                            }}
                            source={Icons.IC_OFFERS}
                        />
                        <Text
                            style={{
                                fontSize: Matrics.mvs(18),
                                fontWeight: 'bold',
                            }}>
                            {'Offers'}
                        </Text>
                        <TouchableOpacity
                            delayPressIn={0}
                            activeOpacity={0.5}
                            style={{
                                marginLeft: Matrics.s(10),
                                justifyContent: 'center',
                                alignItems: 'center',
                                padding: Matrics.mvs(5),
                            }}
                            onPress={() => Navigation.navigate('Checkout')}>
                            <ICFeather
                                name={'shopping-cart'}
                                size={24}
                                color={Colors.PRIMARY_COLOR}
                            />
                            <View
                                style={{
                                    position: 'absolute',
                                    right: 0,
                                    top: 0,
                                    height: Matrics.mvs(15),
                                    paddingHorizontal: Matrics.s(4),
                                    backgroundColor: 'red',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    borderRadius: Matrics.mvs(15 / 2),
                                }}>
                                <Text
                                    style={{
                                        fontSize: Matrics.mvs(10),
                                        color: Colors.WHITE,
                                        fontWeight: 'bold',
                                    }}>
                                    {cart?.length}
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </SafeAreaView>
            <ScrollView
                style={{ flex: 1 }}
                showsVerticalScrollIndicator={false}>
                <View>
                    <FlatList
                        data={offers}
                        showsHorizontalScrollIndicator={false}
                        keyExtractor={(_, index) => index.toString()}
                        renderItem={_renderOffers}
                        horizontal
                        contentContainerStyle={{
                            paddingVertical: Matrics.mvs(16),
                            paddingRight: Matrics.mvs(16),
                        }}
                    />
                </View>
                <View
                    style={{
                        flexDirection: 'row',
                        marginBottom: Matrics.mvs(16),
                    }}>
                    {milestones.map((item, index) => {
                        return (
                            <TouchableOpacity
                                activeOpacity={0.5}
                                delayPressIn={0}
                                key={index}
                                style={{
                                    width:
                                        (Matrics.screenWidth -
                                            Matrics.mvs(16 * 5)) /
                                        4,
                                    borderRadius: Matrics.mvs(4),
                                    marginLeft: Matrics.mvs(16),
                                }}>
                                <View
                                    style={{
                                        position: 'absolute',
                                        width:
                                            (Matrics.screenWidth -
                                                Matrics.mvs(16 * 5)) /
                                            4,
                                        height: Matrics.vs(45),
                                        backgroundColor: Colors.SURFACE,
                                        borderRadius: Matrics.mvs(4),
                                    }}
                                />
                                <View
                                    style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        marginTop: Matrics.vs(8),
                                    }}>
                                    <Image
                                        style={{
                                            height: Matrics.mvs(60),
                                            width: Matrics.mvs(60),
                                            resizeMode: 'contain',
                                        }}
                                        source={item.icon}
                                    />
                                    <Text
                                        style={{
                                            fontSize: Matrics.mvs(11),
                                            textAlign: 'center',
                                            marginTop: Matrics.vs(-3),
                                            flex: 0.6,
                                        }}>
                                        {item.text}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        );
                    })}
                </View>
                <View style={{ flex: 1 }}>
                    {isLoading ? (
                        <LoaderUI />
                    ) : (
                        data.map((item, index) => (
                            <Product
                                item={item}
                                index={index}
                                addedItems={addedItems}
                                onPressAdd={onPressAdd}
                                onPressRemove={onPressRemove}
                            />
                        ))
                    )}
                </View>
            </ScrollView>
            <Loader visible={false} />
        </View>
    );
};

const Product = ({ item, index, addedItems, onPressAdd, onPressRemove }) => {
    const [expanded, setExpanded] = useState(false);

    return (
        <>
            <TouchableOpacity
                key={index}
                activeOpacity={0.5}
                delayPressIn={0}
                style={{
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    paddingHorizontal: Matrics.s(16),
                    paddingVertical: Matrics.vs(8),
                }}
                onPress={() => setExpanded(!expanded)}>
                <View>
                    <Text
                        style={{
                            color: Colors.BLACK,
                            fontSize: Matrics.mvs(16),
                            fontWeight: 'bold',
                        }}>
                        {item.category_name}
                    </Text>
                    <Text
                        style={{
                            color: Colors.BLACK,
                            fontSize: Matrics.mvs(13),
                            fontWeight: '600',
                        }}>
                        {`${item.products.length} Items`}
                    </Text>
                </View>
                <ICFeather
                    name={'chevron-down'}
                    size={28}
                    color={Colors.PRIMARY_COLOR}
                    style={{
                        transform: [{ rotate: expanded ? '0deg' : '180deg' }],
                    }}
                />
            </TouchableOpacity>
            {expanded &&
                item.products.map((product, index) => {
                    return (
                        <View style={styles.itemWrpper} key={index}>
                            <View style={styles.itemImageWrapper}>
                                <Image
                                    source={product.image}
                                    style={styles.itemImage}
                                />
                            </View>
                            <View style={styles.itemContentWrapper}>
                                <Text numberOfLines={1} style={styles.itemName}>
                                    {product.name}
                                </Text>
                                <Text style={styles.itemDesc}>
                                    {product.desc}
                                </Text>
                                <Text style={styles.itemPrice}>
                                    {Number(product.price).toFixed(2)}
                                </Text>
                                <View style={styles.buttonsWrapper}>
                                    <TouchableOpacity
                                        onPress={() => onPressAdd(product)}
                                        style={[
                                            styles.itemButton,
                                            {
                                                backgroundColor:
                                                    Colors.PRIMARY_COLOR,
                                            },
                                        ]}>
                                        <Text
                                            style={[
                                                styles.itemButtonLabel,
                                                { color: Colors.WHITE },
                                            ]}>
                                            {'Add'}
                                        </Text>
                                    </TouchableOpacity>
                                    {addedItems.includes(product.id) && (
                                        <TouchableOpacity
                                            onPress={() =>
                                                onPressRemove(product)
                                            }
                                            style={styles.itemButton}>
                                            <Text
                                                style={styles.itemButtonLabel}>
                                                {'Remove'}
                                            </Text>
                                        </TouchableOpacity>
                                    )}
                                </View>
                            </View>
                        </View>
                    );
                })}
        </>
    );
};

export default Home;
