/**
 * Validate username regex pattern - Boolean
 * @param {String} text - value to test
 */
const validateUsername = (text) =>
    /^[A-Za-z0-9]+(?:[_][A-Za-z0-9]+)*$/.test(text);

/**
 * Validate email regex pattern - Boolean
 * @param {String} text - value to test
 */
const validateEmail = (text) =>
    /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,5})+$/.test(text);

/**
 * Validate password regex pattern - Boolean
 * @param {String} text - value to test
 */
const validatePassword = (text) =>
    /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9_ ]{8,20}$/.test(text);

/**
 * Validate special character not allowed - Boolean
 * @param {String} text - value to test
 */
const validateSpecialCharacter = (text) =>
    /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(text);
// /^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9!@#$%^&*]{8,20}$/.test(
//     text,
// );

/**
 * Validate custom regex pattern - Boolean
 * @param {RegExp} regex - regex pattern
 * @param {String} text - value to test
 */
const validateRegex = (regex, text) => new RegExp(regex).test(text);

export default {
    validateEmail,
    validatePassword,
    validateRegex,
    validateUsername,
    validateSpecialCharacter,
};
