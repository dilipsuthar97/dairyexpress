import { Constants } from '../CommonConfig';

/**
 * Return random number between inclusive and exclusive
 * @param {Number} length - exclusive length for randome number
 * @param {Boolean} countOne - add plus 1 count
 */
const getRandomNumber = (exclusiveLength = 1, countOne = false) => {
    if (countOne) {
        return Math.floor(Math.random() * exclusiveLength + 1);
    } else {
        return Math.floor(Math.random() * exclusiveLength);
    }
};

/**
 * Return a random UUID identifier (always generates random UUID )
 */
const getUUID = () => {
    var dt = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(
        /[xy]/g,
        function (c) {
            var r = (dt + Math.random() * 16) % 16 | 0;
            dt = Math.floor(dt / 16);
            return (c == 'x' ? r : (r & 0x3) | 0x8).toString(16);
        },
    );
    return uuid;
};

/**
 * Return a trimmed string by removing extra spaces
 * @param {String} text - message to be trimmed
 */
const trimString = (text) => {
    if (typeof text == 'string') {
        return text.replace(/\s+/g, ' ').trim();
    }
    return text;
};

/**
 * Return a time difference from current local time
 * @param {Date} current - current date object
 * @param {Date} previous - your custom date object
 */
const timeDifference = (current, previous) => {
    console.log(current, previous);
    var msPerMinute = 60 * 1000;
    var msPerHour = msPerMinute * 60;
    var msPerDay = msPerHour * 24;
    var msPerMonth = msPerDay * 30;
    var msPerYear = msPerDay * 365;

    var elapsed = current - previous;

    if (elapsed < msPerMinute) {
        return Math.round(elapsed / 1000) + 's ago';
    } else if (elapsed < msPerHour) {
        return Math.round(elapsed / msPerMinute) + 'm ago';
    } else if (elapsed < msPerDay) {
        return Math.round(elapsed / msPerHour) + 'h ago';
    } else if (elapsed < msPerMonth) {
        return Math.round(elapsed / msPerDay) + 'd ago';
    } else if (elapsed < msPerYear) {
        return Math.round(elapsed / msPerMonth) + 'm ago';
    } else {
        return Math.round(elapsed / msPerYear) + 'y ago';
    }
};

/**
 * Return type of environemt TESTING | PRODUCTION
 */
const isTestEnvironment = () => {
    return (
        (global.env ?? Constants.ENVIRONMENT.TEST) ===
        Constants.ENVIRONMENT.TEST
    );
};

export default {
    getRandomNumber,
    getUUID,
    trimString,
    timeDifference,
    isTestEnvironment,
};
