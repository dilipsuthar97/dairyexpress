import { Constants } from '../CommonConfig';

const error = (message = Constants.DESC.NETWORK_ALERT) => {
    global._alert.alertWithType('error', 'Error', message);
};

const success = (message) => {
    global._alert.alertWithType('success', 'Success', message);
};

const info = (message) => {
    global._alert.alertWithType('info', 'Info', message);
};

export default {
    error,
    success,
    info,
};
