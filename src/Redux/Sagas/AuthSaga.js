// // --------------- LIBRARIES ---------------
// import { put, call, takeEvery, all } from 'redux-saga/effects';

// // --------------- ASSETS ---------------
// import { Login, SignUp } from '../Types';
// import { login, signUp } from '../Actions';
// import { Auth } from '../../Services';

// const loginSaga = function* loginSaga({ params }) {
//     try {
//         const response = yield call(Auth.Login, params);
//         yield put(login.Success(response));
//     } catch (error) {
//         yield put(login.Failed(error));
//     }
// };

// const signUpSaga = function* signUpSaga({ params }) {
//     try {
//         const response = yield call(Auth.SignUp, params);
//         yield put(signUp.Success(response));
//     } catch (error) {
//         yield put(signUp.Failed(error));
//     }
// };

// function* authSaga() {
//     yield all([
//         takeEvery(Login.REQUEST, loginSaga),
//         takeEvery(SignUp.REQUEST, signUpSaga),
//     ]);
// }

// export default authSaga;
