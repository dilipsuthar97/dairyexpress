import CommonReducer from './CommonReducer';
import AuthReducer, { INITIAL_STATE as INITIAL_AUTH } from './AuthReducer';
import { combineReducers } from 'redux';

import { Logout } from '../Types';

let appReducer = combineReducers({
    Common: CommonReducer,
    Auth: AuthReducer,
});

const rootReducer = (state, action) => {
    if (action.type === Logout.REQUEST) {
        state = {
            Common: state.Common,
            Auth: INITIAL_AUTH,
        };
    }

    return appReducer(state, action);
};

export default rootReducer;
