// --------------- TYPES ---------------
import { GetNetwork, AddToCart, RemoveFromCart, ClearCart } from '../Types';

// --------------- INITIAL STATE ---------------
const INITIAL_STATE = {
    isConnected: false,
    cart: [],
    orders: [],
    error: null,
    errorMsg: '',
    successMsg: '',
};

// --------------- REDUCER FUNCTION ---------------
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GetNetwork.REQUEST:
            return { ...state, isConnected: action.payload };

        case AddToCart.REQUEST:
            return { ...state, cart: [...state.cart, action.payload] };

        case RemoveFromCart.REQUEST:
            return {
                ...state,
                cart: state.cart.filter((c, i) => c.id != action.payload.id),
            };

        case ClearCart.REQUEST:
            return { ...state, cart: [] };

        default:
            return state;
    }
};
