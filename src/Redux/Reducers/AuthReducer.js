// --------------- TYPES ---------------
import { Login, SignUp, Logout } from '../Types';

import { Tools } from '../../Helpers';

// --------------- INITIAL STATE ---------------
export const INITIAL_STATE = {
    error: null,
    errorMsg: '',
    successMsg: '',
    data: { isAuthenticated: false, phone_no: '', full_name: '', email: '' },
};

// --------------- REDUCER FUNCTION ---------------
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        // Login
        case Login.REQUEST:
            return {
                ...state,
                // isLoginSuccess: null,
                // error: null,
                // errorMsg: '',
                // successMsg: '',
                data: {
                    ...action.params,
                    isAuthenticated: true,
                },
            };
        // case Login.SUCCESS:
        //     return {
        //         ...state,
        //         isLoginSuccess: true,
        //         data: {
        //             ...action.payload,
        //             isAuthenticated: true,
        //         },
        //         successMsg: 'Login successfull.',
        //     };
        // case Login.FAILED:
        //     return {
        //         ...state,
        //         isLoginSuccess: false,
        //         error: action.payload,
        //         errorMsg: Tools.trimString(action.payload.message),
        //     };

        // SignUp
        // case SignUp.REQUEST:
        //     return {
        //         ...state,
        //         isSignUpSuccess: null,
        //         error: null,
        //         errorMsg: '',
        //         successMsg: '',
        //     };
        // case SignUp.SUCCESS:
        //     return {
        //         ...state,
        //         isSignUpSuccess: true,
        //         // data: action.payload,
        //         successMsg: 'SignUp successfull.',
        //     };
        // case SignUp.FAILED:
        //     return {
        //         ...state,
        //         isSignUpSuccess: false,
        //         error: action.payload,
        //         errorMsg: Tools.trimString(action.payload.message),
        //     };

        // Logout user
        case Logout.REQUEST:
            return INITIAL_STATE;

        default:
            return state;
    }
};
