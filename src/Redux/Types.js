/**
 * Common Types
 */
export const GetNetwork = {
    REQUEST: 'get_network_request',
};

/**
 * Auth Types
 */
export const SignUp = {
    REQUEST: 'signup_request',
    SUCCESS: 'signup_success',
    FAILED: 'signup_failed',
};

export const Login = {
    REQUEST: 'login_request',
    SUCCESS: 'login_success',
    FAILED: 'login_failed',
};

export const Logout = {
    REQUEST: 'logout_request',
    SUCCESS: 'logout_success',
    FAILED: 'logout_failed',
};

// Cart
export const AddToCart = {
    REQUEST: 'add_to_cart',
};

export const RemoveFromCart = {
    REQUEST: 'remove_from_cart',
};

export const ClearCart = {
    REQUEST: 'clear_cart',
};
