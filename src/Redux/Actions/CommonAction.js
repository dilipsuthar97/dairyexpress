// --------------- TYPES ---------------
import { GetNetwork, AddToCart, RemoveFromCart, ClearCart } from '../Types';

// --------------- ACTIONS ---------------
export const getNetwork = {
    Request: (isConnected) => ({
        type: GetNetwork.REQUEST,
        payload: isConnected,
    }),
};

export const addToCart = {
    Request: (item) => ({
        type: AddToCart.REQUEST,
        payload: item,
    }),
};

export const removeFromCart = {
    Request: (item) => ({
        type: RemoveFromCart.REQUEST,
        payload: item,
    }),
};

export const clearCart = {
    Request: () => ({
        type: ClearCart.REQUEST,
    }),
};
