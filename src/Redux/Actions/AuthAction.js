// --------------- TYPES ---------------
import { Login, SignUp, Logout } from '../Types';

// --------------- ACTIONS ---------------
export const login = {
    Request: (params) => ({
        type: Login.REQUEST,
        params,
    }),
    Success: (data) => ({
        type: Login.SUCCESS,
        payload: data,
    }),
    Failed: (error) => ({
        type: Login.FAILED,
        payload: error,
    }),
};

export const signUp = {
    Request: (params) => ({
        type: SignUp.REQUEST,
        params,
    }),
    Success: (data) => ({
        type: SignUp.SUCCESS,
        payload: data,
    }),
    Failed: (error) => ({
        type: SignUp.FAILED,
        payload: error,
    }),
};

export const logout = {
    Request: (params) => ({
        type: Logout.REQUEST,
        params,
    }),
    Success: (data) => ({
        type: Logout.SUCCESS,
        payload: data,
    }),
    Failed: (error) => ({
        type: Logout.FAILED,
        payload: error,
    }),
};
