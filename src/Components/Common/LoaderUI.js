// --------------- LIBRARIES ---------------
import React from 'react';
import { View, StyleSheet, ActivityIndicator } from 'react-native';

// --------------- ASSETS ---------------
import { Colors, Matrics } from '../../CommonConfig';

// --------------- COMPONENT DECLARATION ---------------
const LoaderUI = ({ indicatorSize, indicatorColor, style }) => {
    return (
        <View style={[styles.wrapper, style]}>
            <ActivityIndicator
                size={indicatorSize ?? 'small'}
                color={indicatorColor ?? Colors.PRIMARY_COLOR}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
});

export default LoaderUI;
