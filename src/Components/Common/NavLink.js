// --------------- LIBRARIES ---------------
import React from 'react';
import { TouchableOpacity, Text, Animated } from 'react-native';

// --------------- ASSETS ---------------
import { Colors, Matrics } from '../../CommonConfig';

// --------------- COMPONENT ---------------
const NavLink = ({
    label,
    onPress,
    fontSize,
    color,
    textDecorationColor,
    textDecorationType,
    style,
}) => {
    return (
        <TouchableOpacity
            delayPressIn={0}
            onPress={onPress}
            activeOpacity={0.5}
            style={style}
            hitSlop={{ top: 10, right: 10, bottom: 10, left: 10 }}>
            <Animated.Text
                style={{
                    fontSize: Matrics.mvs(fontSize ?? 16),
                    color: color ?? Colors.BLACK,
                    textDecorationLine: textDecorationType ?? 'underline',
                    textDecorationColor: textDecorationColor ?? Colors.BLACK,
                }}>
                {label}
            </Animated.Text>
        </TouchableOpacity>
    );
};

export default NavLink;
