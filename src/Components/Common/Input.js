// --------------- LIBRARIES ---------------
import React from 'react';
import {
    TouchableOpacity,
    Text,
    StyleSheet,
    View,
    TextInput,
    Image,
} from 'react-native';

// --------------- ASSETS ---------------
import { Colors, Matrics, Icons } from '../../CommonConfig';

// --------------- COMPONENT ---------------
const Input = (
    {
        value,
        onChangeText,
        editable,
        placeholder,
        keyboardType,
        blurOnSubmit,
        onSubmitEditing,
        returnKeyType,
        secureTextEntry,
        label,
        showPasswordToggle,
        onPasswordToggle,
        errorMsg,
        style,
        autoFocus,
    },
    ref,
) => {
    return (
        <View style={style}>
            <Text style={styles.label}>{label}</Text>
            <View style={styles.inputWrapper(errorMsg)}>
                <TextInput
                    style={styles.input}
                    ref={ref}
                    value={value}
                    onChangeText={onChangeText}
                    editable={editable ?? true}
                    placeholder={placeholder}
                    placeholderTextColor={Colors.LIGHT_GRAY}
                    keyboardType={keyboardType ?? 'default'}
                    blurOnSubmit={blurOnSubmit}
                    onSubmitEditing={onSubmitEditing}
                    returnKeyType={returnKeyType}
                    secureTextEntry={secureTextEntry}
                    autoFocus={autoFocus ?? false}
                />
                {showPasswordToggle && (
                    <TouchableOpacity
                        delayPressIn={0}
                        onPress={onPasswordToggle}
                        activeOpacity={0.5}
                        hitSlop={{ top: 10, right: 10, bottom: 10, left: 10 }}>
                        <Image
                            style={styles.toggleIcon}
                            resizeMode='contain'
                            source={
                                secureTextEntry
                                    ? Icons.IC_EYE_HIDE
                                    : Icons.IC_EYE
                            }
                        />
                    </TouchableOpacity>
                )}
            </View>
            {errorMsg != '' && <Text style={styles.error}>{errorMsg}</Text>}
        </View>
    );
};

const styles = StyleSheet.create({
    container: {},
    labelWrapper: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    icon: {
        height: Matrics.mvs(17),
        width: Matrics.mvs(17),
        tintColor: Colors.FONT_GRAY,
    },
    label: {
        color: Colors.GRAY,
        fontSize: Matrics.mvs(12),
        fontWeight: '700',
        marginBottom: Matrics.vs(3),
    },
    inputWrapper: (error) => ({
        flexDirection: 'row',
        borderColor: error ? Colors.ERROR : Colors.OVERLAY_DARK_20,
        borderWidth: 1,
        borderRadius: Matrics.mvs(4),
        paddingRight: Matrics.s(8),
        alignItems: 'center',
    }),
    input: {
        flex: 1,
        paddingLeft: Matrics.s(8),
        paddingTop: Matrics.vs(4),
        paddingRight: Matrics.s(8),
        paddingBottom: Matrics.vs(4),
        color: Colors.BLACK,
        fontSize: Matrics.mvs(13),
    },
    toggleIcon: {
        height: Matrics.mvs(18),
        width: Matrics.mvs(18),
        tintColor: Colors.BLACK,
    },
    error: {
        color: Colors.ERROR,
        fontSize: Matrics.mvs(12),
        marginTop: Matrics.vs(2),
    },
});

export default React.forwardRef(Input);
