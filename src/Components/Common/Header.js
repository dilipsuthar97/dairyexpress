// --------------- LIBRARIES ---------------
import React from 'react';
import { TouchableOpacity, Text, StyleSheet, View, Image } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import ICFeather from 'react-native-vector-icons/Feather';

// --------------- ASSETS ---------------
import { Colors, Matrics, Icons, MainStyles } from '../../CommonConfig';

// --------------- COMPONENT ---------------
const Header = ({
    style,
    title,
    titleStyle,
    titleWrapperStyle,
    onPressBack,
    backIcon,
    backIconStyle,
    hideBackButton,
    showCloseButton,
    leftButtons,
    rightButtons,
    buttonsWrapperStyle,
    onPressClose,
}) => {
    const insets = useSafeAreaInsets();

    const renderBackButton = () => {
        if (!hideBackButton) {
            return (
                <TouchableOpacity
                    delayPressIn={0}
                    key='button_back'
                    activeOpacity={0.3}
                    onPress={onPressBack}
                    hitSlop={{
                        top: 12,
                        right: 12,
                        bottom: 12,
                        left: 12,
                    }}>
                    {/* <Image
                        source={backIcon ?? Icons.IC_RIGHT_ARROW}
                        resizeMode='contain'
                        style={[styles.backIcon, backIconStyle]}
                    /> */}
                    <ICFeather
                        name={'arrow-left'}
                        size={22}
                        color={Colors.WHITE}
                        style={[styles.backIcon, backIconStyle]}
                    />
                </TouchableOpacity>
            );
        }
    };

    const renderCloseButton = () => {
        if (showCloseButton) {
            return (
                <TouchableOpacity
                    delayPressIn={0}
                    key='button_close'
                    activeOpacity={0.3}
                    onPress={onPressClose}
                    hitSlop={{
                        top: 12,
                        right: 12,
                        bottom: 12,
                        left: 12,
                    }}>
                    <Image
                        source={Icons.IC_CLOSE}
                        resizeMode='contain'
                        style={[styles.closeIcon, backIconStyle]}
                    />
                </TouchableOpacity>
            );
        }
    };

    const renderLeftButtons = () => {
        if (leftButtons) {
            return leftButtons.map((b) => b);
        }
    };

    const renderRightButtons = () => {
        if (rightButtons) {
            return rightButtons.map((b) => b);
        }
    };

    return (
        <View style={[styles.header(insets.top), style]}>
            <View style={[styles.titleWrapper(insets.top), titleWrapperStyle]}>
                <Text
                    numberOfLines={2}
                    ellipsizeMode='head'
                    style={[styles.title(insets.top), titleStyle]}>
                    {title}
                </Text>
            </View>
            <View
                style={[
                    styles.buttonsWrapper(insets.top),
                    buttonsWrapperStyle,
                ]}>
                {renderBackButton()}
                {renderLeftButtons()}
                <View style={{ flex: 1 }} />
                {renderRightButtons()}
                {renderCloseButton()}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    header: (top) => ({
        paddingHorizontal: Matrics.s(16),
        height: Matrics.vs(45) + top,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: Colors.PRIMARY_COLOR,
        ...MainStyles.shadow,
        shadowRadius: Matrics.ms(4),
        shadowOpacity: 0.16,
        paddingTop: top,
        position: 'relative',
    }),
    titleWrapper: (top) => ({
        // justifyContent: 'center',
        // alignItems: 'center',
        flex: 1,
        position: 'absolute',
        alignSelf: 'center',
        // top: top,
        // backgroundColor: 'red',
    }),
    title: (top) => ({
        color: Colors.WHITE,
        fontSize: Matrics.mvs(18),
        textAlign: 'center',
        width: '100%',
        paddingTop: top - 5,
    }),
    buttonsWrapper: (top) => ({
        flexDirection: 'row',
        alignItems: 'center',
        flex: 1,
        // marginTop: top,
    }),
    backIcon: {
        height: Matrics.vs(22),
        width: Matrics.vs(22),
        // tintColor: Colors.BLACK,
    },
    closeIcon: {
        height: Matrics.vs(17),
        width: Matrics.vs(17),
        // tintColor: Colors.BLACK,
    },
    logo: (top) => ({
        // height: Matrics.vs(45) + top,
        width: Matrics.vs(Matrics.screenWidth * 0.41),
        marginTop: top,
    }),
});

export default Header;
