// --------------- LIBRARIES ---------------
import React from 'react';
import {
    TouchableOpacity,
    Text,
    StyleSheet,
    ActivityIndicator,
} from 'react-native';

// --------------- ASSETS ---------------
import { Colors, Matrics, MainStyles } from '../../CommonConfig';

// --------------- COMPONENT ---------------
const Button = ({
    label,
    onPress,
    color,
    backgroundColor,
    style,
    textStyle,
    showActivityIndicator,
    inidicatorColor,
    disabled,
}) => {
    return (
        <TouchableOpacity
            delayPressIn={0}
            onPress={onPress}
            activeOpacity={0.5}
            disabled={disabled}
            style={[
                styles.wrapper,
                {
                    backgroundColor: disabled
                        ? Colors.FONT_GRAY
                        : backgroundColor ?? Colors.PRIMARY_COLOR,
                },
                !disabled && MainStyles.shadow,
                style,
            ]}>
            {showActivityIndicator ? (
                <ActivityIndicator color={inidicatorColor} size='small' />
            ) : (
                <Text
                    style={[
                        styles.label,
                        { color: color ?? Colors.WHITE },
                        textStyle,
                    ]}>
                    {label}
                </Text>
            )}
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        height: Matrics.vs(40),
        borderRadius: Matrics.ms(4),
        justifyContent: 'center',
        alignItems: 'center',
    },
    label: {
        color: Colors.WHITE,
        fontSize: Matrics.mvs(15),
        textAlign: 'center',
        fontWeight: 'bold',
    },
});

export default Button;
