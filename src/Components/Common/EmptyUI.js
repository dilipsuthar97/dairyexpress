// --------------- LIBRARIES ---------------
import React from 'react';
import { View, StyleSheet, Image, Text } from 'react-native';

// --------------- ASSETS ---------------
import { Colors, Matrics, Fonts, Icons } from '../../CommonConfig';
import Header from './Header';
import NavLink from './NavLink';

// --------------- COMPONENT DECLARATION ---------------
const EmptyUI = ({
    color,
    iconColor,
    icon,
    title,
    message,
    // hideHeader,
    // hideCloseButton,
    // showBackButton,
    // onPressClose,
    showIcon,
    showTryAgain,
    onPressTryAgain,
}) => {
    return (
        <>
            {/* {!hideHeader && (
                <Header
                    backIconStyle={{ tintColor: color ?? Colors.WHITE }}
                    hideBackButton={!showBackButton}
                    onPressBack={onPressClose}
                    onPressClose={onPressClose}
                    showCloseButton={!hideCloseButton}
                />
            )} */}
            <View style={styles.wrapper}>
                {showIcon && (
                    <Image
                        source={icon}
                        resizeMode='contain'
                        style={styles.icon(iconColor)}
                    />
                )}
                {(title ?? '') != '' && (
                    <Text style={styles.noItemsText(color)}>
                        {title ?? 'Oh no!'}
                    </Text>
                )}
                {(message ?? '') != '' && (
                    <Text style={styles.noItemsMessage(color)}>
                        {message ??
                            'Something went wrong, please try again later.'}
                    </Text>
                )}
                {showTryAgain && (
                    <NavLink
                        label='Try Again'
                        color={Colors.PRIMARY_COLOR}
                        textDecorationType='none'
                        fontSize={14}
                        style={styles.button}
                        onPress={onPressTryAgain}
                    />
                )}
            </View>
        </>
    );
};

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon: (iconColor) => ({
        width: Matrics.mvs(80),
        height: Matrics.mvs(80),
        tintColor: iconColor ?? Colors.PRIMARY_COLOR,
        marginBottom: Matrics.vs(16),
    }),
    noItemsText: (color) => ({
        fontSize: Matrics.mvs(18),
        color: color ?? Colors.MATEBLACK,
        textAlign: 'center',
        paddingHorizontal: Matrics.s(28),
    }),
    noItemsMessage: (color) => ({
        fontSize: Matrics.mvs(13),
        color: color ?? Colors.FONT_GRAY,
        textAlign: 'center',
        paddingVertical: Matrics.vs(6),
        paddingHorizontal: Matrics.s(28),
    }),
    button: {
        alignSelf: 'center',
        marginTop: Matrics.vs(5),
    },
});

export default EmptyUI;
