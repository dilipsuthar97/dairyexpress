// --------------- LIBRARIES ---------------
import React from 'react';
import { View, Modal, ActivityIndicator, Text } from 'react-native';

// --------------- ASSETS ---------------
import { Colors, Matrics } from '../../CommonConfig';

// --------------- COMPONENT DECLARATION ---------------
const Loader = ({ visible }) => {
    return (
        <Modal visible={visible} transparent statusBarTranslucent={true}>
            <View
                style={{
                    flex: 1,
                    justifyContent: 'center',
                    backgroundColor: Colors.OVERLAY_DARK_70,
                    alignItems: 'center',
                }}>
                <View
                    style={{
                        backgroundColor: Colors.WHITE,
                        borderRadius: Matrics.mvs(8),
                        flexDirection: 'row',
                        alignItems: 'center',
                        paddingHorizontal: Matrics.s(16),
                        paddingVertical: Matrics.vs(8),
                        elevation: 3,
                    }}>
                    <ActivityIndicator
                        size={'large'}
                        color={Colors.PRIMARY_COLOR}
                    />
                    <Text
                        style={{
                            color: Colors.BLACK,
                            fontSize: Matrics.mvs(15),
                            marginLeft: Matrics.s(8),
                        }}>
                        {'Please wait...'}
                    </Text>
                </View>
            </View>
        </Modal>
    );
};

export default Loader;
