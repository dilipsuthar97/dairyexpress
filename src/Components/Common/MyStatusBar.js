import React from 'react';
import { StatusBar } from 'react-native';
import { useIsFocused } from '@react-navigation/native';

// --------------- ASSETS ---------------
import { Colors } from '../../CommonConfig';

// --------------- COMPONENT ---------------
const MyStatusbar = ({ barStyle, backgroundColor, translucent }) => {
    return useIsFocused() ? (
        <StatusBar
            barStyle={barStyle ?? 'dark-content'}
            translucent={translucent ?? true}
            backgroundColor={backgroundColor ?? Colors.TRANSPARENT}
        />
    ) : null;
};

export default MyStatusbar;
