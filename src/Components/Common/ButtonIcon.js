// --------------- LIBRARIES ---------------
import React from 'react';
import { TouchableOpacity, Image, StyleSheet } from 'react-native';

// --------------- ASSETS ---------------
import { Colors, Matrics } from '../../CommonConfig';

// --------------- COMPONENT ---------------
const ButtonIcon = ({ icon, onPress, style, iconStyle, hitSlop }) => {
    return (
        <TouchableOpacity
            delayPressIn={0}
            style={[styles.button, style]}
            onPress={onPress}
            activeOpacity={0.5}
            hitSlop={{
                top: hitSlop?.top ?? 10,
                right: hitSlop?.right ?? 10,
                bottom: hitSlop?.bottom ?? 10,
                left: hitSlop?.left ?? 10,
            }}>
            <Image
                style={[styles.icon, iconStyle]}
                source={icon}
                resizeMode='contain'
            />
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    icon: {
        tintColor: Colors.PRIMARY_COLOR,
        width: Matrics.mvs(26),
        height: Matrics.mvs(26),
        padding: Matrics.mvs(4),
    },
});

export default ButtonIcon;
