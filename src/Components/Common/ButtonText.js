// --------------- LIBRARIES ---------------
import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

// --------------- ASSETS ---------------
import { Colors, Fonts, Matrics } from '../../CommonConfig';

// --------------- COMPONENT ---------------
const ButtonText = ({ text, onPress, style, textStyle, hitSlop }) => {
    return (
        <TouchableOpacity
            delayPressIn={0}
            activeOpacity={0.5}
            style={[styles.button, style]}
            onPress={onPress}
            hitSlop={{
                top: hitSlop?.top ?? 10,
                right: hitSlop?.right ?? 10,
                bottom: hitSlop?.bottom ?? 10,
                left: hitSlop?.left ?? 10,
            }}>
            <Text style={[styles.text, textStyle]}>{text}</Text>
        </TouchableOpacity>
    );
};

const styles = StyleSheet.create({
    button: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    text: {
        color: Colors.PRIMARY_COLOR,
        fontSize: Matrics.mvs(16),
    },
});

export default ButtonText;
