// --------------- LIBRARIES ---------------
import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { DrawerContentScrollView } from '@react-navigation/drawer';
import ICAnt from 'react-native-vector-icons/AntDesign';
import ICEntypo from 'react-native-vector-icons/Entypo';
import ICMaterial from 'react-native-vector-icons/MaterialIcons';

// --------------- ASSETS ---------------
import { Colors, Matrics } from '../CommonConfig';
import { Navigation } from '../Helpers';

// --------------- DRAWER ---------------
const DrawerMenu = ({ navigation, Auth, onLogout }) => {
    const onPressProfile = () => {
        Navigation.toggleDrawer();
        navigation.navigate('Profile');
    };

    const onPressOrderCalendar = () => {
        Navigation.toggleDrawer();
        navigation.navigate('OrderCalendar');
    };

    const onPressSupport = () => {
        Navigation.toggleDrawer();
        navigation.navigate('SupportFaq');
    };

    const onPressLogout = () => {
        Navigation.toggleDrawer();
        onLogout();
    };

    return (
        <DrawerContentScrollView style={styles.wrapper}>
            <TouchableOpacity style={styles.menuItem} onPress={onPressProfile}>
                <ICMaterial
                    name={'account-circle'}
                    size={24}
                    color={Colors.PRIMARY_COLOR}
                    style={styles.icon}
                />
                <Text style={styles.label}>{Auth?.data?.phone_no ?? ''}</Text>
                <ICEntypo
                    name={'chevron-right'}
                    size={22}
                    color={Colors.BLACK}
                    style={styles.iconArrow}
                />
            </TouchableOpacity>
            <View style={styles.devider} />
            <TouchableOpacity
                style={styles.menuItem}
                onPress={onPressOrderCalendar}>
                <ICAnt
                    name={'calendar'}
                    size={18}
                    color={Colors.PRIMARY_COLOR}
                    style={styles.icon}
                />
                <Text style={styles.label}>{'Order calendar'}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuItem}>
                <ICMaterial
                    name={'payment'}
                    size={18}
                    color={Colors.PRIMARY_COLOR}
                    style={styles.icon}
                />
                <Text style={styles.label}>{'Wallet Transactions'}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuItem} onPress={onPressSupport}>
                <ICMaterial
                    name={'support-agent'}
                    size={18}
                    color={Colors.PRIMARY_COLOR}
                    style={styles.icon}
                />
                <Text style={styles.label}>{'Support & FAQs'}</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.menuItem} onPress={onPressLogout}>
                <ICMaterial
                    name={'logout'}
                    size={18}
                    color={Colors.PRIMARY_COLOR}
                    style={styles.icon}
                />
                <Text style={styles.label}>{'Logout'}</Text>
            </TouchableOpacity>
        </DrawerContentScrollView>
    );
};

const styles = StyleSheet.create({
    wrapper: { padding: Matrics.mvs(8) },
    menuItem: {
        flexDirection: 'row',
        paddingHorizontal: Matrics.s(10),
        alignItems: 'center',
        paddingVertical: Matrics.vs(12),
    },
    icon: {
        marginRight: Matrics.s(8),
    },
    label: {
        fontSize: Matrics.mvs(14),
        flex: 1,
        color: Colors.GRAY,
        fontWeight: '600',
    },
    iconArrow: {
        marginLeft: Matrics.s(8),
    },
    devider: {
        height: 1,
        backgroundColor: Colors.OVERLAY_DARK_10,
    },
});

export default DrawerMenu;
