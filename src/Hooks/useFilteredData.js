// --------------- LIBRARIES ---------------
import { useState, useEffect } from 'react';

// --------------- HOOK DECLARATION ---------------
/**
 * Returns a filtered data, and a function to update the main data source.
 *
 * @param {String} query - query which is going to search.
 * @param {Array} keysToSearch - list of keys to search for value.
 */
export default (query, keysToSearch) => {
    const [data, setData] = useState([]);
    const [filteredData, setFilteredData] = useState(data);

    useEffect(() => {
        setFilteredData(
            data.filter((d) =>
                keysToSearch.some(
                    (k) =>
                        (d[k] ?? '')
                            .toLowerCase()
                            .indexOf(query.toLowerCase()) > -1,
                ),
            ),
        );
    }, [query, data]);

    return [filteredData, setData];
};
