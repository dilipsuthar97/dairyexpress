// --------------- LIBRARIES ---------------
import { useRef, useEffect } from 'react';

// --------------- HOOK DECLARATION ---------------
/**
 * Returns a prevous stored value from ref to compare.
 *
 * @param {Object} value - object value to compare prev values.
 */
export default (value) => {
    const ref = useRef();
    useEffect(() => {
        ref.current = value;
    });
    return ref.current;
};
