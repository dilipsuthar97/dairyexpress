import { Images } from '../CommonConfig';

const products = [
    {
        id: '1',
        category_name: 'Full Cream Milk',
        products: [
            {
                id: '1-1',
                name: 'Amul Taaza - Full Cream Milk',
                desc: '1 Packet (1L)',
                price: 58.0,
                image: Images.AMUL_TAAZA,
            },
            {
                id: '1-2',
                name: 'Amul Gold - Full Cream Milk',
                desc: '1 Packet (1L)',
                price: 24.0,
                image: Images.AMUL_GOLD,
            },
            {
                id: '1-3',
                name: 'Amul Shakti - Full Cream Milk',
                desc: '1 Packet (1L)',
                price: 80.0,
                image: Images.AMUL_TAAZA,
            },
        ],
    },
    {
        id: '2',
        category_name: 'Full Cream Milk',
        products: [
            {
                id: '2-1',
                name: 'Amul Taaza - Full Cream Milk',
                desc: '1 Packet (1L)',
                price: 34.0,
                image: Images.AMUL_TAAZA,
            },
            {
                id: '2-2',
                name: 'Amul Taaza - Full Cream Milk',
                desc: '1 Packet (1L)',
                price: 29.0,
                image: Images.AMUL_TAAZA,
            },
            {
                id: '2-3',
                name: 'Amul Taaza - Full Cream Milk',
                desc: '1 Packet (1L)',
                price: 79.0,
                image: Images.AMUL_TAAZA,
            },
            {
                id: '2-4',
                name: 'Amul Taaza - Full Cream Milk',
                desc: '1 Packet (1L)',
                price: 80.0,
                image: Images.AMUL_TAAZA,
            },
        ],
    },
    {
        id: '3',
        category_name: 'Full Cream Milk',
        products: [
            {
                id: '3-1',
                name: 'Amul Taaza - Full Cream Milk',
                desc: '1 Packet (1L)',
                price: 112.0,
                image: Images.AMUL_TAAZA,
            },
            {
                id: '3-2',
                name: 'Amul Taaza - Full Cream Milk',
                desc: '1 Packet (1L)',
                price: 14.0,
                image: Images.AMUL_TAAZA,
            },
        ],
    },
];

export default {
    products,
};
