export default {
    PRIMARY_COLOR: 'rgb(78,173,234)',
    WHITE: '#FFFFFF',
    BLACK: '#000',
    MATEBLACK: '#343434',
    LIGHT_GRAY: '#C0C0C0',
    GRAY: '#808080',
    TRANSPARENT: 'rgba(0,0,0,0.0)',
    ERROR: '#f07970',
    SUCCESS: '#38C1A0',

    COLOR_1: 'rgb(48, 112, 184)',
    COLOR_2: 'rgb(234, 152, 63)',
    COLOR_3: 'rgb(64, 144, 77)',

    SURFACE: 'rgb(242,242,242)',
    LOGO_BG: 'rgb(1,102,176)',

    OVERLAY_DARK_10: 'rgba(0,0,0,0.1)',
    OVERLAY_DARK_20: 'rgba(0,0,0,0.2)',
    OVERLAY_DARK_30: 'rgba(0,0,0,0.3)',
    OVERLAY_DARK_40: 'rgba(0,0,0,0.4)',
    OVERLAY_DARK_50: 'rgba(0,0,0,0.5)',
    OVERLAY_DARK_60: 'rgba(0,0,0,0.6)',
    OVERLAY_DARK_70: 'rgba(0,0,0,0.7)',

    OVERLAY_LIGHT_10: 'rgba(255,255,255,0.1)',
    OVERLAY_LIGHT_20: 'rgba(255,255,255,0.2)',
    OVERLAY_LIGHT_30: 'rgba(255,255,255,0.3)',
    OVERLAY_LIGHT_40: 'rgba(255,255,255,0.4)',
    OVERLAY_LIGHT_50: 'rgba(255,255,255,0.5)',
    OVERLAY_LIGHT_60: 'rgba(255,255,255,0.6)',
};
