import { StyleSheet } from 'react-native';

import Colors from './Colors';
import Matrics from './Matrics';

const MainStyles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: Colors.WHITE,
    },
    backIcon: {
        height: Matrics.ms(18),
        width: Matrics.ms(18),
        tintColor: Colors.WHITE,
    },
    shadow: {
        shadowColor: Colors.BLACK,
        shadowOffset: { width: Matrics.s(0), height: Matrics.vs(2) },
        shadowOpacity: 0.2,
        shadowRadius: Matrics.mvs(5),
        elevation: 4,
    },
    shadowUp: {
        shadowColor: Colors.BLACK,
        shadowOffset: { width: Matrics.s(0), height: Matrics.vs(5) },
        shadowRadius: Matrics.mvs(20),
        shadowOpacity: 0.2,
        elevation: Matrics.vs(15),
    },
});

export default MainStyles;
