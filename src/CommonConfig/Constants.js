import { Platform } from 'react-native';

export default {
    IS_TESTDATA: '1',
    PLACEHOLDER_PROFILE:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSPgYHQLH78Sqfe1-DHsMWe_BVKgWmxj293lw&usqp=CAU',
    PLACEHOLDER_MEN: 'https://randomuser.me/api/portraits/men/35.jpg',
    DEVICE_TYPE: Platform.OS == 'android' ? '2' : '1',
    LINK_T_AND_C: '',
    LINK_PRIVACY_POLICY: '',

    // Validation strings
    FULLNAME_ERROR: {
        EMPTY: 'Please enter full name.',
        INVALID: 'Your full name should contains letters.',
    },
    PASSWORD_ERROR: {
        EMPTY: 'Please enter password.',
        EMPTY_CONFIRM_PASS: 'Please enter confirm password.',
        INVALID:
            'Your password must be at least 8 characters long, should contains at least 1 uppercase, 1 lowercase, 1 numeric value.',
        NO_MATCH: 'Password does not match.',
        NO_SPECIAL_CHARACTER: 'Password should not contain special characters.',
        NEW_PASSWORD_NOT_LAST_PASSWORD:
            'New password should not be last password.',
    },
    EMAIL_ERROR: {
        EMPTY: 'Please enter email.',
        INVALID: 'Please enter valid email address.',
    },
    DESC: {
        NETWORK_ALERT: 'Please connect to a valid internet connection.',
    },
};
