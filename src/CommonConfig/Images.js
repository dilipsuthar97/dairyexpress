export default {
    AMUL_TAAZA: require('../Assets/Images/amul_taaza.png'),
    AMUL_GOLD: require('../Assets/Images/amul_gold.jpg'),
    INTRO_1: require('../Assets/Images/intro_1.png'),
    INTRO_2: require('../Assets/Images/intro_2.png'),
    INTRO_3: require('../Assets/Images/intro_3.png'),
    LOGO: require('../Assets/Images/logo.png'),
    OFFER_1: require('../Assets/Images/offer_1.jpg'),
    OFFER_2: require('../Assets/Images/offer_2.jpg'),
};
