export default {
    IC_CALENDAR: require('../Assets/Icons/ic_calendar.png'),
    IC_EGGS: require('../Assets/Icons/ic_eggs.png'),
    IC_LOCATION_COVER: require('../Assets/Icons/ic_location_cover.png'),
    IC_OFFERS_COVER: require('../Assets/Icons/ic_offers_cover.png'),
    IC_OFFERS: require('../Assets/Icons/ic_offers.png'),
    IC_SECURITY: require('../Assets/Icons/ic_security.png'),
};
