// --------------- LIBRARIES ---------------
import React, { Fragment } from 'react';
import { TextInput, Text, Platform, UIManager } from 'react-native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import DropdownAlert from 'react-native-dropdownalert';

// --------------- ASSETS ---------------
import Routes from './Routes';
import { Store, Persistor } from './Redux/Store';
import { Colors, Matrics } from './CommonConfig';

/**
 * Font scalling configuration for Android :)
 */
if (Text.defaultProps == null) Text.defaultProps = {};
Text.defaultProps.allowFontScaling = false;
if (TextInput.defaultProps == null) TextInput.defaultProps = {};
TextInput.defaultProps.allowFontScaling = false;

/**
 * Animation configuration for Android
 */
if (Platform.OS === 'android') {
    if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
}

// --------------- MAIN ---------------
export default () => {
    return (
        <Fragment>
            <Provider store={Store}>
                <PersistGate persistor={Persistor}>
                    <Routes />
                    <DropdownAlert
                        ref={(ref) => (global._alert = ref)}
                        closeInterval={800}
                        translucent={true}
                        activeStatusBarStyle='light-content'
                        inactiveStatusBarStyle='dark-content'
                        inactiveStatusBarBackgroundColor={Colors.TRANSPARENT}
                        titleStyle={{
                            fontSize: Matrics.mvs(16),
                            color: Colors.WHITE,
                            fontWeight: 'bold',
                        }}
                        messageStyle={{
                            fontSize: Matrics.mvs(12),
                            color: Colors.WHITE,
                        }}
                    />
                </PersistGate>
            </Provider>
        </Fragment>
    );
};
