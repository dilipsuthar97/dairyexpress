// --------------- LIBRARIES ---------------
import 'react-native-gesture-handler';
import * as React from 'react';
import { NavigationContainer, CommonActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import NetInfo from '@react-native-community/netinfo';
import { useDispatch, useSelector } from 'react-redux';

// --------------- ASSETS ---------------
import { navigationRef } from './Helpers/Navigation';
import { getNetwork, logout } from './Redux/Actions';
import DrawerMenu from './Components/DrawerMenu';

// --------------- SCREENS ---------------
import Splash from './Screens/Auth/Splash';
import Onboarding from './Screens/Auth/Onboarding';
import Login from './Screens/Auth/Login';
import Verification from './Screens/Auth/Verification';
import Home from './Screens/Home/Home';
import Profile from './Screens/Home/Profile';
import SelectLocation from './Screens/Home/SelectLocation';
import OrderCalendar from './Screens/Home/OrderCalendar';
import Checkout from './Screens/Home/Checkout';
import SupportFaq from './Screens/Home/SupportFaq';
import { Matrics } from './CommonConfig';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

// --------------- DRAWER TAB BAR ---------------
const DrawerNavigator = (props) => {
    const { Auth } = useSelector((state) => state);
    const dispatch = useDispatch();

    const onLogout = () => {
        dispatch(logout.Request());
        props.navigation.dispatch(
            CommonActions.reset({
                index: 1,
                routes: [{ name: 'Onboarding' }],
            }),
        );
    };

    return (
        <Drawer.Navigator
            drawerType={Matrics.screenWidth >= 768 ? 'permanent' : 'front'}
            drawerContent={(props) => (
                <DrawerMenu {...props} Auth={Auth} onLogout={onLogout} />
            )}>
            <Drawer.Screen name='Home' component={Home} />
        </Drawer.Navigator>
    );
};

// --------------- ROUTES ---------------
const Routes = () => {
    // --------------- REDUCER STATE ---------------
    const dispatch = useDispatch(); // dispatch method to dispatch our actions to reducer and saga

    // --------------- LIFECYCLE ---------------
    React.useEffect(() => {
        const unsubscribe = NetInfo.addEventListener((state) => {
            dispatch(getNetwork.Request(state.isConnected));
        });

        return () => {
            unsubscribe();
        };
    }, []);

    // --------------- RENDER ---------------
    return (
        <SafeAreaProvider>
            <NavigationContainer ref={navigationRef}>
                <Stack.Navigator initialRouteName='Splash'>
                    <Stack.Screen
                        name='Splash'
                        options={{ headerShown: false }}
                        component={Splash}
                    />
                    <Stack.Screen
                        name='Onboarding'
                        options={{ headerShown: false }}
                        component={Onboarding}
                    />
                    <Stack.Screen
                        name='Login'
                        options={{ headerShown: false }}
                        component={Login}
                    />
                    <Stack.Screen
                        name='Verification'
                        options={{ headerShown: false }}
                        component={Verification}
                    />
                    <Stack.Screen
                        name='SelectLocation'
                        options={{ headerShown: false }}
                        component={SelectLocation}
                    />
                    <Stack.Screen
                        name='Profile'
                        options={{ headerShown: false }}
                        component={Profile}
                    />
                    <Stack.Screen
                        name='OrderCalendar'
                        options={{ headerShown: false }}
                        component={OrderCalendar}
                    />
                    <Stack.Screen
                        name='Checkout'
                        options={{ headerShown: false }}
                        component={Checkout}
                    />
                    <Stack.Screen
                        name='SupportFaq'
                        options={{ headerShown: false }}
                        component={SupportFaq}
                    />
                    <Stack.Screen
                        name='Root'
                        options={{ headerShown: false }}
                        component={DrawerNavigator}
                    />
                </Stack.Navigator>
            </NavigationContainer>
        </SafeAreaProvider>
    );
};

export default Routes;
